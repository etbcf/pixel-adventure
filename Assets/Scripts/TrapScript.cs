using System.Collections;
using UnityEngine;

public class TrapScript : MonoBehaviour
{
    [SerializeField] private Transform[] waypoints; // Array of waypoints for the trap to move between
    [SerializeField] private float moveSpeed = 15f; // Speed at which the trap moves
    [SerializeField] private float moveDelay = 1f; // Delay between movements

    private int currentWaypointIndex = 0; // Index of the current waypoint

    // Start is called before the first frame update
    void Start()
    {
        // Start the movement coroutine
        StartCoroutine(MoveRoutine());
    }

    /// <summary>
    /// Coroutine for the movement of the trap.
    /// </summary>
    IEnumerator MoveRoutine()
    {
        while (true)
        {
            // Move the object towards the current waypoint
            transform.position = Vector3.MoveTowards(transform.position, waypoints[currentWaypointIndex].position, moveSpeed * Time.deltaTime);

            // Check if the object has reached the current waypoint
            if (Vector3.Distance(transform.position, waypoints[currentWaypointIndex].position) < 0.1f)
            {
                // If true, move to the next waypoint
                currentWaypointIndex++;

                // If the current waypoint index exceeds the array length, reset it to 0
                if (currentWaypointIndex >= waypoints.Length)
                {
                    currentWaypointIndex = 0;
                }

                // Wait for moveDelay seconds before moving again
                yield return new WaitForSeconds(moveDelay);
            }

            yield return null;
        }
    }
}
