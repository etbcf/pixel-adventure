﻿using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// Represents a sound clip with associated properties such as volume, pitch, and loop.
/// </summary>
[System.Serializable]
public class Sound {

    public string name;  // Name of the sound

    public AudioClip clip;  // Audio clip

    public AudioMixerGroup mixer;  // Audio mixer group for this sound

    [Range(0f, 1f)]
    public float volume = 1;  // Volume of the sound

    [Range(-3f, 3f)]
    public float pitch = 1;  // Pitch of the sound

    public bool loop = false;  // Whether the sound should loop

    [HideInInspector]
    public AudioSource source;  // Reference to the AudioSource component

}
