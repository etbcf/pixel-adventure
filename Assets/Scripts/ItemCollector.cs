using UnityEngine;

public class ItemCollector : MonoBehaviour
{
    [SerializeField] int pointsForItemPickup = 100;

    /// <summary>
    /// Called when the Collider2D other enters the trigger.
    /// </summary>
    /// <param name="collision">The other Collider2D involved in this collision.</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Items"))
        {
            GameSession gameSession = FindObjectOfType<GameSession>();
            if (gameSession != null)
            {
                gameSession.AddToScore(pointsForItemPickup);
                AudioManager.instance.Play("Items");
                Destroy(collision.gameObject);
            }
        }
    }
}
