using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// CameraController class responsible for controlling the camera's position.
/// </summary>
public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform player; // Reference to the player's transform

    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    void Update()
    {
        // Check if the player transform is not null before accessing its position
        if (player != null)
        {
            // Update the camera position to follow the player on the X and Y axes
            transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
        }
    }
}
