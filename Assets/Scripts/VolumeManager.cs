using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeManager : MonoBehaviour
{
    [SerializeField] Slider volumeSlider; // Reference to the volume slider UI element

    // Start is called before the first frame update
    void Start()
    {
        // Check if the volume setting is not yet saved
        if (!PlayerPrefs.HasKey("Volume"))
        {
            // Default volume to 50%
            PlayerPrefs.SetFloat("Volume", 0.5f);
            Load(); // Load volume settings
        }
        else
        {
            Load(); // Load volume settings
        }
    }

    /// <summary>
    /// Method to change the volume when the slider value changes.
    /// </summary>
    public void ChangeVolume()
    {
        // Set the volume of the audio listener to the slider value
        AudioListener.volume = volumeSlider.value;

        // Save the volume setting to persist the changes
        Save();
    }

    /// <summary>
    /// Load volume settings from player preferences.
    /// </summary>
    private void Load()
    {
        // Load the volume setting from player preferences
        volumeSlider.value = PlayerPrefs.GetFloat("Volume");
        
        // Set the volume of the audio listener to match the loaded volume setting
        AudioListener.volume = volumeSlider.value;

        // Set the slider's normalized value to match the loaded volume
        volumeSlider.normalizedValue = volumeSlider.value;
    }

    /// <summary>
    /// Save the current volume setting to player preferences.
    /// </summary>
    private void Save()
    {
        // Save the current volume setting to player preferences
        PlayerPrefs.SetFloat("Volume", volumeSlider.value);
    }
}
