using System.Collections;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private SpriteRenderer sprite;
    private Animator anim;
    private BoxCollider2D myBodyCollider;

    [SerializeField] private LayerMask jumpableGround;

    private float dirX = 0f;
    [SerializeField] private float moveSpeed = 7f;
    [SerializeField] private float jumpForce = 14f;
    [SerializeField] private float fallThreshold = -10f;

    private enum MovementState { idle, running, jumping, falling }

    bool isAlive = true;
    bool dead = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        myBodyCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (transform.position.y < -25f && !dead)
        {
            Die();
        }

        if (!isAlive) { return; }

        dirX = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(dirX * moveSpeed, rb.velocity.y);

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            // Call the Jump method when the Jump button is pressed and the player is grounded
            Jump();
            AudioManager.instance.Play("Jump");
        }

        UpdateAnimationState();
    }

    /// <summary>
    /// Jump method to make the player character jump.
    /// </summary>
    public void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
    }

    /// <summary>
    /// Updates the animation state based on the player's movement and velocity.
    /// </summary>
    private void UpdateAnimationState()
    {
        MovementState state;

        if (dirX > 0f)
        {
            state = MovementState.running;
            sprite.flipX = false;
        }
        else if (dirX < 0f)
        {
            state = MovementState.running;
            sprite.flipX = true;
        }
        else
        {
            state = MovementState.idle;
        }

        if (rb.velocity.y > .1f)
        {
            state = MovementState.jumping;
        }
        else if (rb.velocity.y < -.1f)
        {
            state = MovementState.falling;
        }

        anim.SetInteger("state", (int)state);
    }

    /// <summary>
    /// Checks if the player is grounded by performing a boxcast downwards.
    /// </summary>
    /// <returns>True if the player is grounded, false otherwise.</returns>
    private bool IsGrounded()
    {
        return Physics2D.BoxCast(myBodyCollider.bounds.center, myBodyCollider.bounds.size, 0f, Vector2.down, .1f, jumpableGround);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (isAlive && (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Traps")))
        {
            Die();
        }
    }

    /// <summary>
    /// Handles player death by triggering the death animation and destroying the player object.
    /// </summary>
    void Die()
    {
        if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Enemies", "Traps")) || transform.position.y < fallThreshold)
        {
            isAlive = false;
            anim.SetTrigger("dying");
            AudioManager.instance.Play("Death");
            StartCoroutine(DestroyPlayerAfterAnimation());
        }

        dead = true;
    }

    /// <summary>
    /// Destroys the player object after a delay to ensure the "dying" animation has finished playing.
    /// </summary>
    /// <returns>An enumerator to manage coroutine execution.</returns>
    IEnumerator DestroyPlayerAfterAnimation()
    {
        // Wait for a fixed duration to ensure the "dying" animation has time to play
        yield return new WaitForSeconds(1.0f);

        FindObjectOfType<GameSession>().ProcessPlayerDeath();
        // Destroy the player object after the animation has (presumably) finished
        Destroy(gameObject);
    }
}
