using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneExit : MonoBehaviour
{
    [SerializeField] float sceneLoadDelay = 1f;

    /// <summary>
    /// Called when a GameObject collides with the trigger collider attached to this GameObject.
    /// Initiates the coroutine to load the next scene after a delay.
    /// </summary>
    /// <param name="collision">The Collider2D component attached to the GameObject that collided with the trigger.</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(LoadNextScene());
    }

    /// <summary>
    /// Coroutine to load the next scene after a delay.
    /// </summary>
    /// <returns>An enumerator to manage coroutine execution.</returns>
    IEnumerator LoadNextScene()
    {
        yield return new WaitForSecondsRealtime(sceneLoadDelay);

        // Play the "End" sound here
        AudioManager.instance.Play("End");

        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;

        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0;
        }

        FindObjectOfType<ScenePersist>().ResetScenePersist();
        SceneManager.LoadScene(currentSceneIndex + 1);
    }
}
