using System;
using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// AudioManager class responsible for managing audio in the game.
/// </summary>
public class AudioManager : MonoBehaviour {

	/// <summary>
	/// Static instance of the AudioManager class.
	/// </summary>
	public static AudioManager instance;

	/// <summary>
	/// Array of Sound objects representing different sounds in the game.
	/// </summary>
	public Sound[] sounds;
	
	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake ()
	{
		if (instance != null)
		{
			Destroy(gameObject);
			return;
		} else
		{
			instance = this;
			DontDestroyOnLoad(transform.root.gameObject);
		}

		foreach (Sound s in sounds)
		{
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.volume = s.volume;
			s.source.pitch = s.pitch;
			s.source.loop = s.loop;
		}
	}

	/// <summary>
	/// Play the specified sound.
	/// </summary>
	/// <param name="sound">Name of the sound to play.</param>
	public void Play(string sound)
	{
		Sound s = Array.Find(sounds, item => item.name == sound);
		s.source.Play();
	}

	/// <summary>
	/// Stop the specified sound.
	/// </summary>
	/// <param name="sound">Name of the sound to stop.</param>
	public void Stop(string sound)
	{
		Sound s = Array.Find(sounds, item => item.name == sound);
		s.source.Stop();
	}
}

