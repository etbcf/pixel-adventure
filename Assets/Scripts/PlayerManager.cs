using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    public GameObject[] playerPrefabs;

    int characterIndex;

    public GameObject pauseMenuScreen;

    public CinemachineVirtualCamera vCam;

    /// <summary>
    /// Awake is called before the first frame update.
    /// Initializes the selected character and sets up the camera to follow the player.
    /// </summary>
    void Awake()
    {
        characterIndex = PlayerPrefs.GetInt("SelectedCharacter", 0);
        GameObject player = Instantiate(playerPrefabs[characterIndex]);
        vCam.m_Follow = player.transform;
    }

    /// <summary>
    /// Pauses the game by setting the time scale to 0 and activating the pause menu screen.
    /// </summary>
    public void PauseGame()
    {
        Time.timeScale = 0;
        pauseMenuScreen.SetActive(true);
    }

    /// <summary>
    /// Resumes the game by setting the time scale to 1 and deactivating the pause menu screen.
    /// </summary>
    public void ResumeGame()
    {
        Time.timeScale = 1;
        pauseMenuScreen.SetActive(false);
    }

    /// <summary>
    /// Loads the main menu scene.
    /// </summary>
    public void GoToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
