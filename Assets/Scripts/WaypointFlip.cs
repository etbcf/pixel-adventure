using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFlip : MonoBehaviour
{
    [SerializeField] private GameObject[] waypoints; // Array of waypoints for the object to move between
    [SerializeField] private float speed = 2f; // Speed at which the object moves

    private int currentWaypointIndex = 0; // Index of the current waypoint

    private bool movingRight = true; // Flag to determine the direction of movement

    // Update is called once per frame
    private void Update()
    {
        // Check if the object has reached the current waypoint
        if (Vector2.Distance(waypoints[currentWaypointIndex].transform.position, transform.position) < 0.1f)
        {
            // If true, move to the next waypoint
            currentWaypointIndex++;

            // If the current waypoint index exceeds the array length, reset it to 0
            if (currentWaypointIndex >= waypoints.Length)
            {
                currentWaypointIndex = 0;
            }

            // Change the direction of movement
            FlipDirection();
        }

        // Move the object towards the current waypoint using MoveTowards method
        transform.position = Vector2.MoveTowards(transform.position, waypoints[currentWaypointIndex].transform.position, Time.deltaTime * speed);
    }

    /// <summary>
    /// Method to change the direction of movement and flip the object horizontally.
    /// </summary>
    private void FlipDirection()
    {
        // Change the direction of movement
        movingRight = !movingRight;

        // Flip the object horizontally by changing its scale
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}
