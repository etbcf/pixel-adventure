using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// GameSession class responsible for managing the game session, player lives, and score.
/// </summary>
public class GameSession : MonoBehaviour
{
    [SerializeField] int playerLives = 3; // Initial number of player lives
    [SerializeField] int score = 0; // Initial score
    [SerializeField] TextMeshProUGUI livesText; // Reference to the TextMeshProUGUI for displaying lives
    [SerializeField] TextMeshProUGUI scoreText; // Reference to the TextMeshProUGUI for displaying score
    public GameObject gameOverScreen; // Reference to the game over screen

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        // Ensure only one instance of GameSession exists throughout the game
        int numGameSessions = FindObjectsOfType<GameSession>().Length;
        if (numGameSessions > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    /// <summary>
    /// Start is called before the first frame update.
    /// </summary>
    private void Start()
    {
        // Initialize lives and score display
        livesText.text = playerLives.ToString();
        scoreText.text = score.ToString();
    }

    /// <summary>
    /// Get the current score.
    /// </summary>
    public int GetScore()
    {
        return score;
    }

    /// <summary>
    /// Process player death.
    /// </summary>
    public void ProcessPlayerDeath()
    {
        if (playerLives > 1)
        {
            TakeLife();
        }
        else
        {
            GameOver();
        }
    }

    /// <summary>
    /// Add points to the score.
    /// </summary>
    public void AddToScore(int pointsToAdd)
    {
        score += pointsToAdd;
        scoreText.text = score.ToString();
    }

    /// <summary>
    /// Reduce player lives by one and reload the current scene.
    /// </summary>
    private void TakeLife()
    {
        playerLives--;
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
        livesText.text = playerLives.ToString();
    }

    /// <summary>
    /// Display the game over screen and pause the game.
    /// </summary>
    private void GameOver()
    {
        gameOverScreen.SetActive(true); // Activate the game over screen
        FindObjectOfType<ScenePersist>().ResetScenePersist();
        Time.timeScale = 0f; // Pause the game
    }

    /// <summary>
    /// Restart the game session.
    /// </summary>
    public void RestartGame()
    {
        gameOverScreen.SetActive(false);
        ResetGame();
        SceneManager.LoadScene(1);
        Time.timeScale = 1f;
    }

    /// <summary>
    /// Reset the game session to its initial state.
    /// </summary>
    public void ResetGame()
    {
        playerLives = 3;
        score = 0;
        livesText.text = playerLives.ToString();
        scoreText.text = score.ToString();
    }
}
