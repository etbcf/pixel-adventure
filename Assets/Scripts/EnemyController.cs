using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// EnemyController class responsible for controlling enemy behavior.
/// </summary>
public class EnemyController : MonoBehaviour
{
    /// <summary>
    /// Method to kill the enemy by destroying its game object.
    /// </summary>
    public void Kill()
    {
        AudioManager.instance.Play("Enemy-Death");
        Destroy(gameObject);
    }
}
