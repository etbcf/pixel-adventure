using UnityEngine;

/// <summary>
/// EnemyKiller class responsible for killing enemies and awarding points.
/// </summary>
public class EnemyKiller : MonoBehaviour
{
    [SerializeField] int pointsForEnemyKill = 100; // Set the points for killing an enemy

    /// <summary>
    /// Triggered when a collider enters the trigger zone.
    /// </summary>
    /// <param name="collision">The collider that entered the trigger zone.</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Check if the collided object is tagged as an enemy
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // Get the EnemyController component of the collided enemy
            var enemy = collision.gameObject.GetComponent<EnemyController>();
            
            // Kill the enemy
            enemy.Kill();

            // Award points for killing the enemy
            GameSession gameSession = FindObjectOfType<GameSession>();
            if (gameSession != null)
            {
                gameSession.AddToScore(pointsForEnemyKill);
            }

            // Make the player character jump using the public method from PlayerMovement
            PlayerMovement playerMovement = FindObjectOfType<PlayerMovement>();
            if (playerMovement != null)
            {
                playerMovement.Jump();
            }
        }
    }
}
