using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// EndSceneScoreDisplay class responsible for displaying the score on the end scene.
/// </summary>
public class EndSceneScoreDisplay : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText; // Reference to the TextMeshProUGUI component for displaying the score

    /// <summary>
    /// Start is called before the first frame update.
    /// </summary>
    void Start()
    {
        // Find the GameSession object in the scene
        GameSession gameSession = FindObjectOfType<GameSession>();
        
        // Check if the GameSession object is found
        if (gameSession != null)
        {
            // Get the score from the GameSession
            int score = gameSession.GetScore();
            
            // Update the score text to display the score
            scoreText.text = "Score: " + score.ToString();
        }
    }
}
