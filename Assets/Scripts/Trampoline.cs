using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour
{
    private Animator anim;
    public float jumpForce;

    // Start is called before the first frame update
    void Start()
    {
        // Getting the Animator component attached to this GameObject
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // No update logic for the Trampoline class
    }

    /// <summary>
    /// Called when a collision is detected between this trampoline and another collider.
    /// If the colliding object is tagged as "Player", triggers the jump animation and adds a vertical impulse force to the player.
    /// </summary>
    /// <param name="collision">The Collision2D data associated with the collision.</param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            // Triggering the "Jump" animation
            anim.SetTrigger("Jump");
            
            // Adding a vertical impulse force to the player
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
        }
    }
}
