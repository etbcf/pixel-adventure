using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// CharacterSelect class manages the selection of character skins.
/// </summary>
public class CharacterSelect : MonoBehaviour
{
    /// <summary>
    /// Array of GameObjects representing different character skins.
    /// </summary>
    public GameObject[] skins;

    /// <summary>
    /// Index of the currently selected character.
    /// </summary>
    public int selectedCharacter;

    // Start is called before the first frame update
    void Awake()
    {
        // Retrieve the index of the selected character from PlayerPrefs or set to default (0)
        selectedCharacter = PlayerPrefs.GetInt("SelectedCharacter", 0);
        
        // Deactivate all character skins
        foreach (GameObject player in skins)
            player.SetActive(false);

        // Activate the selected character skin
        skins[selectedCharacter].SetActive(true);
    }

    /// <summary>
    /// Change to the next character skin.
    /// </summary>
    public void ChangeNext()
    {
        // Deactivate the current character skin
        skins[selectedCharacter].SetActive(false);
        
        // Increment the selected character index
        selectedCharacter++;
        
        // Loop back to the first character if index exceeds the length of skins array
        if (selectedCharacter == skins.Length)
            selectedCharacter = 0;

        // Activate the newly selected character skin
        skins[selectedCharacter].SetActive(true);
        
        // Save the selected character index to PlayerPrefs
        PlayerPrefs.SetInt("SelectedCharacter", selectedCharacter);
    }

    /// <summary>
    /// Change to the previous character skin.
    /// </summary>
    public void ChangePrevious()
    {
        // Deactivate the current character skin
        skins[selectedCharacter].SetActive(false);
        
        // Decrement the selected character index
        selectedCharacter--;
        
        // Loop back to the last character if index goes below 0
        if (selectedCharacter == -1)
            selectedCharacter = skins.Length - 1;

        // Activate the newly selected character skin
        skins[selectedCharacter].SetActive(true);
        
        // Save the selected character index to PlayerPrefs
        PlayerPrefs.SetInt("SelectedCharacter", selectedCharacter);
    }
}
