using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenePersist : MonoBehaviour
{
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// Checks if there is more than one ScenePersist object in the scene.
    /// If there is more than one, destroys the duplicate object. Otherwise, marks the object to persist across scene loads.
    /// </summary>
    void Awake()
    {
        int numScenePersist = FindObjectsOfType<ScenePersist>().Length;
        if (numScenePersist > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    /// <summary>
    /// Destroys the ScenePersist object.
    /// </summary>
    public void ResetScenePersist()
    {
        Destroy(gameObject);
    }
}
