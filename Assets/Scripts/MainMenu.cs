using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    /// <summary>
    /// Handles the action when the user clicks to go back to the home screen.
    /// </summary>
    public void ClickToHome()
    {
        // Reset game-specific variables
        ResetGame();

        // Load the main menu scene (scene 0)
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Resets game-specific variables, such as the game over screen, player lives, score, etc.
    /// </summary>
    private void ResetGame()
    {
        // Deactivate the game over screen if it's active
        GameSession gameSession = FindObjectOfType<GameSession>();
        if (gameSession != null && gameSession.gameOverScreen != null && gameSession.gameOverScreen.activeSelf)
        {
            gameSession.gameOverScreen.SetActive(false);
            // Reset time scale
            Time.timeScale = 1f;
        }

        // Reset player lives, score, and any other relevant game variables
        if (gameSession != null)
        {
            gameSession.ResetGame();
        }
    }
}
