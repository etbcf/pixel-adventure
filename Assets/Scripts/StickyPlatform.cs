using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyPlatform : MonoBehaviour
{
    /// <summary>
    /// Called when a collision is detected between this platform and another collider.
    /// If the colliding object is tagged as "Player", sets this platform as its parent.
    /// </summary>
    /// <param name="collision">The Collision2D data associated with the collision.</param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            // Setting the platform as the parent of the player object
            collision.gameObject.transform.SetParent(transform);
        }
    }

    /// <summary>
    /// Called when a collision between this platform and another collider ends.
    /// If the colliding object is tagged as "Player", removes this platform as its parent.
    /// </summary>
    /// <param name="collision">The Collision2D data associated with the collision.</param>
    private void OnCollisionExit2D(Collision2D collision)
    {
        // Checking if the colliding object is named "Player"
        if (collision.gameObject.CompareTag("Player"))
        {
            // Removing the platform as the parent of the player object (setting it to null)
            collision.gameObject.transform.SetParent(null);
        }
    }
}
